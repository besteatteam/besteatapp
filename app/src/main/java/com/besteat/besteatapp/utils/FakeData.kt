package com.besteat.besteatapp.utils

import com.besteat.besteatapp.models.*
import com.besteat.besteatapp.ui.faqs.Faq

class FakeData {
    companion object {

        fun getMeals(): ArrayList<Meal>{
            var meals: ArrayList<Meal> = ArrayList()


            meals.add(Meal("Meal A", "blaaaaaaaaaaaaaaa", 4F, "img", MealType("mealtype1", Cuisine("Maroc", "img")),
                Restaurant("Resaurant1","","img"), false))

            meals.add(Meal("Meal B", "blaaaaaaaaaaaaaaa", 3.8F, "img", MealType("mealtype1", Cuisine("Maroc", "img")),
                Restaurant("Resaurant1","","img"), false))

            meals.add(Meal("Meal C", "blaaaaaaaaaaaaaaa", 5.8F, "img", MealType("mealtype1", Cuisine("Maroc", "img")),
                Restaurant("Resaurant1","","img"), true))

            meals.add(Meal("Meal D", "blaaaaaaaaaaaaaaa", 2.8F, "img", MealType("mealtype1", Cuisine("Maroc", "img")),
                Restaurant("Resaurant1","","img"), false))

            for(i in 1..5)
                meals.add(Meal("Meal A", "blaaaaaaaaaaaaaaa", 3.5F, "img", MealType("mealtype1", Cuisine("Maroc", "img")),
                        Restaurant("Resaurant1","","img"), false))

            meals.add(Meal("Last Meal", "blaaaaaaaaaaaaaaa", 2.8F, "img", MealType("mealtype1", Cuisine("Maroc", "img")),
                    Restaurant("Resaurant1","","img"), true))



            return meals
        }

        fun getOffers(): List<Offer>{
            var offers: ArrayList<Offer> = ArrayList()

            offers.add(Offer("title 2","ddddddd","1102001", null))
            offers.add(Offer("title 3","ddddddd","1102001", null))
            offers.add(Offer("title 4","ddddddd","1102001", null))
            offers.add(Offer("title 5","ddddddd","1102001", null))
            offers.add(Offer("title 6","ddddddd","1102001", null))

            for(i in 1..4)
                offers.add(Offer("title x","ddddddd","1102001", null))

            offers.add(Offer("Last Offer","ddddddd","1102001", null))


            return offers

        }

        fun getCuisines(): ArrayList<Cuisine>{
            var cuisines: ArrayList<Cuisine> = ArrayList()

            cuisines.add(Cuisine("Cuisine A", "img"))
            cuisines.add(Cuisine("Cuisine B", "img"))
            cuisines.add(Cuisine("Cuisine C", "img"))
            cuisines.add(Cuisine("Cuisine D", "img"))

            for(i in 1..5)
                cuisines.add(Cuisine("CCCC", "img"))

            cuisines.add(Cuisine("Last Cuisine", "img"))

            return cuisines
        }

        fun getMealTypes(): ArrayList<MealType>{

            var mealTypes: ArrayList<MealType> = ArrayList()

            mealTypes.add(MealType("MealType1", Cuisine("Maroc", "img")))
            mealTypes.add(MealType("MealType2", Cuisine("Maroc", "img")))
            mealTypes.add(MealType("MealType3", Cuisine("Maroc", "img")))
            mealTypes.add(MealType("MealType4", Cuisine("Maroc", "img")))
            mealTypes.add(MealType("MealType5", Cuisine("Maroc", "img")))
            mealTypes.add(MealType("MealType6", Cuisine("Maroc", "img")))

            mealTypes.add(MealType("Last MealType", Cuisine("Maroc", "img")))

            return mealTypes
        }


        fun getFaqs(): ArrayList<Faq> {
            var faqs: ArrayList<Faq> = ArrayList()

            faqs.add(Faq("How does this app protect my privacy ?", "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout hence the point of using letters and phrases"))
            faqs.add(Faq("Am i able to delete my account ?", ""))
            faqs.add(Faq("What happens to my data if i delete my account ?", ""))
            faqs.add(Faq("Does this app track my location ?", ""))
            faqs.add(Faq("Does this app track my location ?", ""))
            faqs.add(Faq("Does this app track my location ?", ""))
            faqs.add(Faq("Does this app track my location ?", ""))

            return faqs
        }

    }
}