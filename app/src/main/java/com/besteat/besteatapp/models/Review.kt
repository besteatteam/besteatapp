package com.besteat.besteatapp.models

data class Review(
    val rate: Float,
    val title: String,
    val comment: String,
    val local: Local?,
    val user: User?
)