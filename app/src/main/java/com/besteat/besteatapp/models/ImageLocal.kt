package com.besteat.besteatapp.models

data class ImageLocal(
    val name: String,
    val local: Local
)