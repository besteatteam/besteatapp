package com.besteat.besteatapp.models

data class Offer(
    val title: String,
    val description: String,
    val deadline: String,
    val local: Local?
)