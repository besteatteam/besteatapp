package com.besteat.besteatapp.models

data class User(
    val name: String,
    val phone: String,
    val email: String,
    val password: String,
    val photo: String
)