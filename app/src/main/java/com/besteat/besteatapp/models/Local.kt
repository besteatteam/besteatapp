package com.besteat.besteatapp.models

import java.sql.Time

data class Local(
    val address: String,
    val city: String,
    val description: String,
    val geo_lt: String,
    val geo_lg: String,
    val phone: String,
    val status: Boolean,
    val delivery_status: Boolean,
    val delivery_price: Float,
    val open_time: String,
    val close_time: String,
    val restaurant: Restaurant
)