package com.besteat.besteatapp.models

data class MealType(
    val name: String,
    val cuisine: Cuisine
)