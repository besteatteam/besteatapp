package com.besteat.besteatapp.models

data class Meal(
    val name: String,
    val description: String,
    val price: Float,
    val image: String,
    val mealType: MealType,
    val restaurant: Restaurant,
    val isBookmarked: Boolean
)