package com.besteat.besteatapp.models

data class Cuisine(
    val name: String,
    val image: String
)