package com.besteat.besteatapp.models

data class Restaurant(
    val name: String,
    val description: String,
    val logo: String
)