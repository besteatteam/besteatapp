package com.besteat.besteatapp.ui.base

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.besteat.besteatapp.R
import com.besteat.besteatapp.ui.home.HomeActivity
import com.besteat.besteatapp.ui.map.MapActivity
import com.besteat.besteatapp.ui.offers.OffersActivity
import com.besteat.besteatapp.ui.settings.SettingsActivity

abstract class BaseActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    protected lateinit var navBottomView: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getContentViewId())

        navBottomView = findViewById(R.id.navBottomView) as BottomNavigationView
        navBottomView.setOnNavigationItemSelectedListener(this)

    }

    override fun onStart() {
        super.onStart()
        updateNavigationBarState()
    }

    // Remove inter-activity transition to avoid screen tossing on tapping bottom navigation items
    public override fun onPause() {
        super.onPause()
        overridePendingTransition(0, 0)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        navBottomView.postDelayed({

            when(item.itemId){
                R.id.nav_bottom_act_home -> {
                    startActivity(Intent(this, HomeActivity::class.java))
                }

                R.id.nav_bottom_act_offers -> {
                    startActivity(Intent(this, OffersActivity::class.java))
                }

                R.id.nav_bottom_act_map -> {
                    startActivity(Intent(this, MapActivity::class.java))
                }

                R.id.nav_bottom_act_settings -> {
                    startActivity(Intent(this, SettingsActivity::class.java))
                }
            }

            finish()
        }, 50)
        return true
    }

    private fun updateNavigationBarState() {
        val actionId = getNavigationMenuItemId()
        selectBottomNavigationBarItem(actionId)
    }

    fun selectBottomNavigationBarItem(itemId: Int) {
        val item = navBottomView.menu.findItem(itemId)
        item.isChecked = true
    }

    abstract fun getContentViewId(): Int

    abstract fun getNavigationMenuItemId(): Int

}
