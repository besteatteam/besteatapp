package com.besteat.besteatapp.ui.auth

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.besteat.besteatapp.R
import kotlinx.android.synthetic.main.activity_main.*

class AuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tabAdapter = TabAdapter(supportFragmentManager)

        viewpager_main.adapter = tabAdapter

        tabs_main.setupWithViewPager(viewpager_main)

    }
}