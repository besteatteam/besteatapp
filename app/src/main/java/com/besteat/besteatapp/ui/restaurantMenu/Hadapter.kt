package com.besteat.besteatapp.ui.restaurantMenu

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.besteat.besteatapp.R
import com.besteat.besteatapp.models.Meal


class Hadapter(var mealList: ArrayList<Meal>) : RecyclerView.Adapter<Hadapter.ViewHolder>() {

    val TAG = "Hadapter"
    var onItemClick: ((Meal) -> Unit)? = null

    lateinit var context: Context

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        Log.e(TAG, "onCreateViewHolder")
        context = p0.context
        val v = LayoutInflater.from(context).inflate(R.layout.cardview_rmenu_meal, p0, false)
        return ViewHolder(v)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return mealList.size
    }


    //this method is binding the data on the list
    override fun onBindViewHolder(holder: Hadapter.ViewHolder, position: Int) {
        Log.e(TAG, "onBindViewHolder")
        holder.bindItems(mealList[position])
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val TAG2 = "Hadapter.ViewHolder"

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(mealList[adapterPosition])
            }
        }

        fun bindItems(meal: Meal) {
            fillListeItems(meal)
        }


        fun fillListeItems(meal: Meal) {
            Log.e(TAG2, "fillListeItems")

            // meal name
            val meal_name = itemView.findViewById(R.id.label_meal_name) as TextView
            meal_name.text = meal.name


        }
    }

}