package com.besteat.besteatapp.ui.offers

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.LinearLayout
import com.besteat.besteatapp.R
import com.besteat.besteatapp.models.Offer
import com.besteat.besteatapp.ui.base.BaseActivity
import com.besteat.besteatapp.utils.FakeData
import kotlinx.android.synthetic.main.activity_offers_content.*



class OffersActivity : BaseActivity() {

    val TAG = "OffersActivity"

    lateinit var adapter: OffersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        init()
    }

    override fun getContentViewId(): Int {
        return R.layout.activity_offers
    }

    override fun getNavigationMenuItemId(): Int {
        return R.id.nav_bottom_act_settings
    }

    fun init(){

        rvOffers.isNestedScrollingEnabled = false
        rvOffers.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        //creating our adapter
        adapter = OffersAdapter(ArrayList())

        //now adding the adapter to recyclerview
        rvOffers.adapter = adapter

        // fake data
        loadOffers()

    }

    fun loadOffers() {
        writeToLog("loadOffers")

        adapter.offersList = FakeData.getOffers() as ArrayList<Offer>
        adapter.notifyDataSetChanged()

        adapter.onItemClick = {
            writeToLog("adapter.onItemClick : " + it.title)
        }
    }

    fun writeToLog(str: String){
        Log.e(TAG, str)
    }
}
