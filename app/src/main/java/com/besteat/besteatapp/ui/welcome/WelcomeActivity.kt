package com.besteat.besteatapp.ui.welcome

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.besteat.besteatapp.R
import com.besteat.besteatapp.ui.auth.AuthActivity
import com.besteat.besteatapp.ui.home.HomeActivity
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        init_navigation_linking()
    }

    fun init_navigation_linking(){

        btn_signIn.setOnClickListener {
            startActivity(Intent(this, AuthActivity::class.java))
        }

        btn_facebook.setOnClickListener {
            Toast.makeText(this, "btn_facebook", Toast.LENGTH_SHORT).show()
        }

        btn_skip.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
        }

    }
}
