package com.besteat.besteatapp.ui.faqs

data class Faq(val question: String, val answer: String)