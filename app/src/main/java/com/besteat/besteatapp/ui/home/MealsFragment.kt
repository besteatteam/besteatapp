package com.besteat.besteatapp.ui.home

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.besteat.besteatapp.R
import com.besteat.besteatapp.models.Cuisine
import com.besteat.besteatapp.models.Meal
import com.besteat.besteatapp.models.MealType
import com.besteat.besteatapp.models.Restaurant
import com.besteat.besteatapp.utils.FakeData

class MealsFragment: Fragment(){

    val TAG = "MealsFragment"

    lateinit var recyclerView: RecyclerView

    lateinit var adapter: MealsAdapter

    companion object {
        fun newInstance(): MealsFragment {
            return MealsFragment()
        }
    }

    override fun onStart() {
        super.onStart()
        writeToLog("onStart()")

        init()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        writeToLog("onCreateView()")

        return inflater.inflate(R.layout.fragment_meals_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        writeToLog("onViewCreated()")

        recyclerView = view.findViewById(R.id.rvMeals) as RecyclerView
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)

        //creating our adapter
        adapter = MealsAdapter(ArrayList())

        //now adding the adapter to recyclerview
        recyclerView.adapter = adapter

    }

    fun init() {
        writeToLog("init()")

        loadMeals()
    }

    fun loadMeals() {
        writeToLog("loadMeals")

        adapter.mealsList = FakeData.getMeals()
        adapter.notifyDataSetChanged()

        adapter.onItemClick = {
            Log.e(TAG, "adapter.onItemClick : " + it.name)
        }
    }

    fun writeToLog(str: String){
        Log.e(TAG, str)
    }
}