package com.besteat.besteatapp.ui.restaurantMenu

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.widget.LinearLayout
import com.besteat.besteatapp.R
import com.besteat.besteatapp.utils.FakeData
import kotlinx.android.synthetic.main.activity_rmenu.*

class RMenuActivity : AppCompatActivity() {

    val TAG = "RMenuActivity"

    lateinit var adapter: Vadapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rmenu)

        init()

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_left_blue)

        loadMealTypes()
    }

    fun init(){
        writeToLog("init")

        rvMealTypes.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        //creating our verticalAdapter
        adapter = Vadapter(ArrayList())

        //now adding the verticalAdapter to recyclerview
        rvMealTypes.adapter = adapter
    }

    fun loadMealTypes(){
        writeToLog("loadMealTypes")


        adapter.mealtypesList = FakeData.getMealTypes()
        adapter.notifyDataSetChanged()

        adapter.onItemClick = {
            writeToLog("adapter.onItemClick : " + it.name)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item?.itemId){

            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)

    }

    fun writeToLog(str: String){
        Log.e(TAG, str)
    }
}
