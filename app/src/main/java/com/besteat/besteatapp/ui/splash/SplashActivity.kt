package com.besteat.besteatapp.ui.splash

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.besteat.besteatapp.R
import com.besteat.besteatapp.ui.home.HomeActivity
import com.besteat.besteatapp.ui.welcome.WelcomeActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        startActivity(Intent(this, WelcomeActivity::class.java))
    }
}
