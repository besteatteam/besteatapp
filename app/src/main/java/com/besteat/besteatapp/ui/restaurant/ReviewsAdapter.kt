package com.besteat.besteatapp.ui.restaurant

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import com.besteat.besteatapp.R
import com.besteat.besteatapp.models.Review
import android.graphics.PorterDuff
import android.support.v4.content.ContextCompat
import android.graphics.drawable.LayerDrawable




class ReviewsAdapter(var reviewList: ArrayList<Review>) : RecyclerView.Adapter<ReviewsAdapter.ViewHolder>() {

    val TAG = "ReviewsAdapter"
    var onItemClick: ((Review) -> Unit)? = null

    lateinit var context: Context

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        Log.e(TAG, "onCreateViewHolder")
        context = p0.context
        val v = LayoutInflater.from(context).inflate(R.layout.cardview_review, p0, false)
        return ViewHolder(v)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return reviewList.size
    }


    //this method is binding the data on the list
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ReviewsAdapter.ViewHolder, position: Int) {
        Log.e(TAG, "onBindViewHolder")
        holder.bindItems(reviewList[position])
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val TAG2 = "ReviewsAdapter.ViewHolder"

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(reviewList[adapterPosition])
            }
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        fun bindItems(review: Review) {
            fillListeItems(review)
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        @SuppressLint("LongLogTag")
        fun fillListeItems(review: Review) {
            Log.e(TAG2, "fillListeItems")

            val rating_stars = itemView.findViewById(R.id.rating_stars) as RatingBar
            rating_stars.rating = review.rate

        }
    }

}