package com.besteat.besteatapp.ui.faqs

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.besteat.besteatapp.R

class FaqsAdapter(var faqsList: ArrayList<Faq>) : RecyclerView.Adapter<FaqsAdapter.ViewHolder>() {


    val TAG = "FaqsAdapter"
    var onItemClick: ((Faq) -> Unit)? = null

    lateinit var context: Context

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        Log.e(TAG, "onCreateViewHolder")
        context = p0.context
        val v = LayoutInflater.from(context).inflate(R.layout.cardview_faqs, p0, false)
        return ViewHolder(v)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return faqsList.size
    }


    //this method is binding the data on the list
    override fun onBindViewHolder(holder: FaqsAdapter.ViewHolder, position: Int) {
        Log.e(TAG, "onBindViewHolder")
        holder.bindItems(faqsList[position])
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val TAG2 = "FaqsAdapter.ViewHolder"

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(faqsList[adapterPosition])
            }
        }

        fun bindItems(faq: Faq) {

        }

    }
}