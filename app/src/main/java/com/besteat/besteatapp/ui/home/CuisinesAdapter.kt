package com.besteat.besteatapp.ui.home

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.besteat.besteatapp.R
import com.besteat.besteatapp.models.Cuisine
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.support.v4.graphics.drawable.RoundedBitmapDrawable
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AppCompatActivity


class CuisinesAdapter(var cuisineList: ArrayList<Cuisine>) : RecyclerView.Adapter<CuisinesAdapter.ViewHolder>() {

    val TAG = "CuisinesAdapter"
    var onItemClick: ((Cuisine) -> Unit)? = null

    lateinit var context: Context

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        Log.e(TAG, "onCreateViewHolder")
        context = p0.context
        val v = LayoutInflater.from(context).inflate(R.layout.cardview_cuisine, p0, false)
        return ViewHolder(v)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return cuisineList.size
    }


    //this method is binding the data on the list
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: CuisinesAdapter.ViewHolder, position: Int) {
        Log.e(TAG, "onBindViewHolder")
        holder.bindItems(cuisineList[position])
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val TAG2 = "CuisinesAdapter.ViewHolder"

        val textViewCuisineName = itemView.findViewById(R.id.txt_cuisine_name) as TextView
        val textViewMealsCount = itemView.findViewById(R.id.txt_meals_count) as TextView
        val imageViewCuisine = itemView.findViewById(R.id.img_cuisine) as View

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(cuisineList[adapterPosition])

                showMealsFragment()
            }
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        fun bindItems(cuisine: Cuisine) {
            fillListeItems(cuisine)
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        @SuppressLint("LongLogTag")
        fun fillListeItems(cuisine: Cuisine) {
            Log.e(TAG2, "fillListeItems")

            imageViewCuisine.clipToOutline = true
            textViewCuisineName.text = cuisine.name
            textViewMealsCount.text = "5"

        }

        fun showMealsFragment() {
            val transaction = (context as AppCompatActivity).supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragment_container, MealsFragment.newInstance())
            transaction.commit()
        }
    }

}