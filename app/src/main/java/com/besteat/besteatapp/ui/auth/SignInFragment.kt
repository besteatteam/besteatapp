package com.besteat.besteatapp.ui.auth

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.besteat.besteatapp.R
import com.besteat.besteatapp.ui.home.HomeActivity
import kotlinx.android.synthetic.main.fragment_sign_in.*

class SignInFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init_navigation_linking()
    }

    fun init_navigation_linking(){

        btn_continue.setOnClickListener {
            context?.startActivity(Intent(context, HomeActivity::class.java))
        }
    }
}