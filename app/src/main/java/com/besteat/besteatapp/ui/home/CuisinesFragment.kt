package com.besteat.besteatapp.ui.home

import android.content.Intent
import android.support.v4.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.besteat.besteatapp.R
import com.besteat.besteatapp.models.Cuisine
import com.besteat.besteatapp.utils.FakeData
import kotlinx.android.synthetic.main.fragment_cuisines_list.*


class CuisinesFragment : Fragment() {

    val TAG = "CuisinesFragment"

    lateinit var recyclerView: RecyclerView

    lateinit var adapter: CuisinesAdapter

    companion object {
        fun newInstance(): CuisinesFragment {
            return CuisinesFragment()
        }
    }

    override fun onStart() {
        super.onStart()
        Log.e(TAG, "onStart")

        init()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.e(TAG, "onCreateView")

        return inflater.inflate(R.layout.fragment_cuisines_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.e(TAG, "onViewCreated")

        recyclerView = view.findViewById(R.id.rvCuisines) as RecyclerView
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)

        //creating our adapter
        adapter = CuisinesAdapter(ArrayList())

        //now adding the adapter to recyclerview
        recyclerView.adapter = adapter

    }

    fun init() {
        Log.e(TAG, "init")

        loadCuisines()
    }

    fun loadCuisines() {
        Log.e(TAG, "loadCuisines")

        adapter.cuisineList = FakeData.getCuisines()
        adapter.notifyDataSetChanged()

        adapter.onItemClick = {
            Log.e(TAG, "adapter.onItemClick : " + it.name)
        }
    }

    fun writeToLog(str: String){
        Log.e(TAG, str)
    }

}