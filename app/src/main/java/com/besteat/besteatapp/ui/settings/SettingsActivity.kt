package com.besteat.besteatapp.ui.settings

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.besteat.besteatapp.R
import com.besteat.besteatapp.ui.base.BaseActivity
import com.besteat.besteatapp.ui.faqs.FaqsActivity
import com.besteat.besteatapp.ui.profile.ProfileActivity
import kotlinx.android.synthetic.main.activity_settings_content.*

class SettingsActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        init()
    }

    override fun getContentViewId(): Int {
        return R.layout.activity_settings
    }

    override fun getNavigationMenuItemId(): Int {
        return R.id.nav_bottom_act_settings
    }

    fun init(){

        btn_faqs.setOnClickListener {
            startActivity(Intent(this, FaqsActivity::class.java))
        }

        btn_profile.setOnClickListener {
            startActivity(Intent(this, ProfileActivity::class.java))
        }

    }
}
