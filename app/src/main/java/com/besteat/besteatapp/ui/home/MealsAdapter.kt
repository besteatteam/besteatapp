package com.besteat.besteatapp.ui.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.besteat.besteatapp.R
import com.besteat.besteatapp.models.Cuisine
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.support.v4.graphics.drawable.RoundedBitmapDrawable
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.widget.ImageButton
import android.widget.RatingBar
import com.besteat.besteatapp.models.Meal
import com.besteat.besteatapp.ui.restaurant.RestaurantActivity


class MealsAdapter(var mealsList: ArrayList<Meal>) : RecyclerView.Adapter<MealsAdapter.ViewHolder>() {

    val TAG = "MealsAdapter"
    var onItemClick: ((Meal) -> Unit)? = null

    lateinit var context: Context

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        Log.e(TAG, "onCreateViewHolder")
        context = p0.context
        val v = LayoutInflater.from(context).inflate(R.layout.cardview_meal, p0, false)
        return ViewHolder(v)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return mealsList.size
    }


    //this method is binding the data on the list
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: MealsAdapter.ViewHolder, position: Int) {
        Log.e(TAG, "onBindViewHolder")
        holder.bindItems(mealsList[position])
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val TAG2 = "MealsAdapter.ViewHolder"

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(mealsList[adapterPosition])

                context.startActivity(Intent(context, RestaurantActivity::class.java))
            }
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        fun bindItems(meal: Meal) {
            fillListeItems(meal)
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        @SuppressLint("LongLogTag", "SetTextI18n")
        fun fillListeItems(meal: Meal) {
            Log.e(TAG2, "fillListeItems")

            val txtMealName = itemView.findViewById(R.id.txt_meal_name) as TextView
            val txtMealDesc = itemView.findViewById(R.id.txt_desc_meal) as TextView
            val txtMealPrice = itemView.findViewById(R.id.txt_price_meal) as TextView
            val txtMealDistance = itemView.findViewById(R.id.txt_distance_meal) as TextView
            val ratingBarMeal = itemView.findViewById(R.id.rating_stars_meal) as RatingBar
            val ratingNumMeal = itemView.findViewById(R.id.rating_num_meal) as TextView
            val bookmarkMealBtn = itemView.findViewById(R.id.bookmark_meal) as ImageButton

            txtMealName.text = meal.name
            txtMealDesc.text = meal.description
            txtMealPrice.text = meal.price.toString() + " MAD"
            txtMealDistance.text = "900" + "m"
            ratingBarMeal.rating = 4F
            ratingNumMeal.text = "("+ "200" +")"

            if(meal.isBookmarked)
                bookmarkMealBtn.setBackgroundResource(R.drawable.ic_bookmark)

        }


    }

}