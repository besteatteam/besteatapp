package com.besteat.besteatapp.ui.home

import android.content.res.Resources
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.besteat.besteatapp.R
import kotlinx.android.synthetic.main.dialog_filters.*
import kotlinx.android.synthetic.main.dialog_filters.view.*
import java.lang.Exception
import java.lang.NullPointerException

class FiltersDialog: DialogFragment() {

    val TAG = "FiltersDialog"

    val SORT_BY_DISTANCE = 1
    val SORT_BY_RATING = 2
    val SORT_BY_PRICEDOWN = 3
    val SORT_BY_PRICEUP = 4
    val SORT_BY_POPULARITY = 5

    var _sortBy: Int = SORT_BY_POPULARITY

    companion object {
        fun newInstance(): FiltersDialog {
            return FiltersDialog()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.dialog_filters, container, false)

        initListener(view)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog.setTitle("FF")
        //dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        init()

        init_navigation_linking()

    }

    fun init(){
        activeSortBy(SORT_BY_POPULARITY)
    }

    fun initListener(view: View){

        ///

        view.sortby_distance_btn.setOnClickListener {
            activeSortBy(SORT_BY_DISTANCE)
        }

        view.sortby_rating_btn.setOnClickListener {
            activeSortBy(SORT_BY_RATING)
        }

        view.sortby_pricedown_btn.setOnClickListener {
            activeSortBy(SORT_BY_PRICEDOWN)
        }

        view.sortby_priceup_btn.setOnClickListener {
            activeSortBy(SORT_BY_PRICEUP)
        }

        view.sortby_popularity_btn.setOnClickListener {
            activeSortBy(SORT_BY_POPULARITY)
        }

        ///


    }


    // Sort by - active
    fun activeSortBy(sortby: Int){

        when(sortby){

            SORT_BY_DISTANCE -> {
                refreshSortBy()
                _sortBy = SORT_BY_DISTANCE
                sortby_distance_btn.setImageResource(R.drawable.ic_filters_distance_on)
                sortby_distance_txt.setTextColor(resources.getColor(R.color.darkblue))

            }

            SORT_BY_RATING -> {
                refreshSortBy()
                _sortBy = SORT_BY_RATING
                sortby_rating_btn.setImageResource(R.drawable.ic_filters_rating_on)
                sortby_rating_txt.setTextColor(resources.getColor(R.color.darkblue))

            }

            SORT_BY_PRICEDOWN -> {
                refreshSortBy()
                _sortBy = SORT_BY_PRICEDOWN
                sortby_pricedown_btn.setImageResource(R.drawable.ic_filters_pricedown_on)
                sortby_pricedown_txt.setTextColor(resources.getColor(R.color.darkblue))

            }

            SORT_BY_PRICEUP -> {
                refreshSortBy()
                _sortBy = SORT_BY_PRICEUP
                sortby_priceup_btn.setImageResource(R.drawable.ic_filters_priceup_on)
                sortby_priceup_txt.setTextColor(resources.getColor(R.color.darkblue))

            }

            SORT_BY_POPULARITY -> {
                refreshSortBy()
                _sortBy = SORT_BY_POPULARITY
                sortby_popularity_btn.setImageResource(R.drawable.ic_filters_popularity_on)
                sortby_popularity_txt.setTextColor(resources.getColor(R.color.darkblue))

            }
        }

        writeToLog("sortby : $sortby")

    }

    // Sort by - disable all ui
    fun refreshSortBy(){

        // disable distance
        sortby_distance_btn.setImageResource(R.drawable.ic_filters_distance_off)
        sortby_distance_txt.setTextColor(resources.getColor(R.color.gris))
        // disable rating
        sortby_rating_btn.setImageResource(R.drawable.ic_filters_rating_off)
        sortby_rating_txt.setTextColor(resources.getColor(R.color.gris))
        // disable pricedown
        sortby_pricedown_btn.setImageResource(R.drawable.ic_filters_pricedown_off)
        sortby_pricedown_txt.setTextColor(resources.getColor(R.color.gris))
        // disable priceup
        sortby_priceup_btn.setImageResource(R.drawable.ic_filters_priceup_off)
        sortby_priceup_txt.setTextColor(resources.getColor(R.color.gris))
        // disable popularity
        sortby_popularity_btn.setImageResource(R.drawable.ic_filters_popularity_off)
        sortby_popularity_txt.setTextColor(resources.getColor(R.color.gris))

    }

    // LOG
    fun writeToLog(str: String){
        Log.e(TAG, str)
    }

    fun init_navigation_linking(){

        btn_reset.setOnClickListener {
            dismiss()
        }

        btn_apply.setOnClickListener {
            dismiss()
        }
    }
}