package com.besteat.besteatapp.ui.restaurant

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.RatingBar
import com.besteat.besteatapp.R
import com.besteat.besteatapp.models.Restaurant
import com.besteat.besteatapp.ui.restaurant.images_card.CardPagerAdapter
import com.besteat.besteatapp.ui.restaurant.images_card.ShadowTransformer
import kotlinx.android.synthetic.main.activity_restaurant.*

class RestaurantActivity : AppCompatActivity(){


    val TAG = "RestaurantActivity"

    private lateinit var mCardAdapter: CardPagerAdapter
    private lateinit var mCardShadowTransformer: ShadowTransformer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant)

        init_toolbar()
        init_head_images()
        init_tabs()
        showInfoFragment()


        //
        label_resto_name.bringToFront()
        val rating_stars = findViewById(R.id.rating_resto) as RatingBar
        rating_stars.rating = 4.3F

    }

    fun init_toolbar(){
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white)
    }

    fun init_head_images(){
        mCardAdapter = CardPagerAdapter()

        mCardAdapter.addCardItem(Restaurant("Resaurant1","q","img"))
        mCardAdapter.addCardItem(Restaurant("Resaurant1","q","img"))
        mCardAdapter.addCardItem(Restaurant("Resaurant1","q","img"))
        mCardAdapter.addCardItem(Restaurant("Resaurant1","q","img"))
        mCardAdapter.addCardItem(Restaurant("Resaurant1","q","img"))
        mCardAdapter.addCardItem(Restaurant("Resaurant1","q","img"))


        mCardShadowTransformer = ShadowTransformer(viewPagerImg, mCardAdapter)
        mCardShadowTransformer.enableScaling(true)

        viewPagerImg.adapter = mCardAdapter
        viewPagerImg.setPageTransformer(false, mCardShadowTransformer)
        viewPagerImg.offscreenPageLimit = 3
        viewPagerImg.pageMargin = 100
    }

    fun init_tabs(){

        btn_informations.setOnClickListener {
            btn_informations.bringToFront()

            btn_informations.setBackgroundResource(R.drawable.btn_restaurant_red)
            btn_reviews.setBackgroundResource(R.drawable.btn_tab_resto_blue)

            showInfoFragment()

        }

        btn_reviews.setOnClickListener {
            btn_reviews.bringToFront()

            btn_reviews.setBackgroundResource(R.drawable.btn_restaurant_red)
            btn_informations.setBackgroundResource(R.drawable.btn_tab_resto_blue)

            showReviewsFragment()
        }

    }


    // init_container_fragments

    fun showInfoFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, InformationsFragment.newInstance())
        transaction.commit()
    }
    fun showReviewsFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, ReviewsFragments.newInstance())
        transaction.commit()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item?.itemId){

            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)

    }

    fun writeToLog(str: String){
        Log.e(TAG, str)
    }
}
