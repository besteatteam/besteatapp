package com.besteat.besteatapp.ui.restaurantMenu

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.besteat.besteatapp.R
import com.besteat.besteatapp.models.Meal
import com.besteat.besteatapp.models.MealType
import com.besteat.besteatapp.utils.FakeData
import kotlinx.android.synthetic.main.cardview_rmenu_mealtype.view.*


class Vadapter(var mealtypesList: ArrayList<MealType>) : RecyclerView.Adapter<Vadapter.ViewHolder>() {

    val TAG = "Vadapter"
    var onItemClick: ((MealType) -> Unit)? = null

    lateinit var context: Context

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        Log.e(TAG, "onCreateViewHolder")
        context = p0.context
        val v = LayoutInflater.from(context).inflate(R.layout.cardview_rmenu_mealtype, p0, false)
        return ViewHolder(v)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return mealtypesList.size
    }

    fun writeToLog(str: String){
        Log.e(TAG, str)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: Vadapter.ViewHolder, position: Int) {
        Log.e(TAG, "onBindViewHolder")
        holder.bindItems(mealtypesList[position])
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val TAG2 = "Vadapter.ViewHolder"

        // meals recycler
        val rvMeals = itemView.findViewById(R.id.rvMeals) as RecyclerView
        //creating our verticalAdapter
        var adapter = Hadapter(ArrayList())

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(mealtypesList[adapterPosition])
            }
        }

        fun bindItems(mealType: MealType) {
            fillListeItems(mealType)
        }


        fun fillListeItems(mealType: MealType) {
            Log.e(TAG2, "fillListeItems")

            // mealtype name
            val mealtype_name = itemView.findViewById(R.id.label_mealtype_name) as TextView
            mealtype_name.text = mealType.name




            rvMeals.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)

            //now adding the verticalAdapter to recyclerview
            rvMeals.adapter = adapter

            loadMeals()
        }

        fun loadMeals(){
            writeToLog2("loadMeals")

            adapter.mealList = FakeData.getMeals()
            adapter.notifyDataSetChanged()

            adapter.onItemClick = {
                writeToLog("adapter.onItemClick : " + it.name)
            }
        }

        fun writeToLog2(str: String){
            Log.e(TAG2, str)
        }
    }

}