package com.besteat.besteatapp.ui.faqs

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.MenuItem
import android.widget.LinearLayout
import com.besteat.besteatapp.R
import com.besteat.besteatapp.utils.FakeData
import kotlinx.android.synthetic.main.activity_faqs.*

class FaqsActivity : AppCompatActivity() {

    val TAG = "FaqsActivity"

    lateinit var adapter: FaqsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_faqs)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_left_blue)

        init()

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item?.itemId){

            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)

    }

    fun init(){

        rvFaqs.isNestedScrollingEnabled = false
        rvFaqs.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        //creating our adapter
        adapter = FaqsAdapter(ArrayList())

        //now adding the adapter to recyclerview
        rvFaqs.adapter = adapter

        // fake data
        loadFaqs()

    }

    fun loadFaqs() {
        writeToLog("loadFaqs")

        adapter.faqsList = FakeData.getFaqs()
        adapter.notifyDataSetChanged()

        adapter.onItemClick = {
            writeToLog("adapter.onItemClick : " + it.question)
        }
    }

    fun writeToLog(str: String){
        Log.e(TAG, str)
    }

}