package com.besteat.besteatapp.ui.restaurant

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.besteat.besteatapp.R
import com.besteat.besteatapp.ui.restaurantMenu.RMenuActivity
import kotlinx.android.synthetic.main.fragment_restaurant_info.*

class InformationsFragment: Fragment() {

    val TAG = "InformationsFragment"


    companion object {
        fun newInstance(): InformationsFragment {
            return InformationsFragment()
        }
    }

    override fun onStart() {
        super.onStart()
        writeToLog("onStart")

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        writeToLog("onCreateView")

        return inflater.inflate(R.layout.fragment_restaurant_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        writeToLog("onViewCreated")

        init_navigation_linking()
    }

    fun writeToLog(str: String){
        Log.e(TAG, str)
    }


    fun init_navigation_linking(){

        btn_menu.setOnClickListener {
            context?.startActivity(Intent(context, RMenuActivity::class.java))
        }

    }
}