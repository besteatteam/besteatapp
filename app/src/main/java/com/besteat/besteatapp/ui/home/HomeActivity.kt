package com.besteat.besteatapp.ui.home

import android.annotation.TargetApi
import android.content.Context
import android.os.Bundle
import com.besteat.besteatapp.R
import com.besteat.besteatapp.ui.base.BaseActivity
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.Intent
import android.graphics.Point
import android.graphics.Rect
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat.getSystemService
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.besteat.besteatapp.utils.KeyboardUtils
import android.view.MotionEvent
import android.view.View.OnTouchListener
import android.widget.FrameLayout
import android.support.v4.content.ContextCompat.getSystemService
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.cardview_searchbar.*


class HomeActivity : BaseActivity() {

    val TAG = "HomeActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        showCuisineFragment()
        //showMealsFragment()
        //showFiltersDialog()

    }

    override fun getContentViewId(): Int {
        return R.layout.activity_home
    }

    override fun getNavigationMenuItemId(): Int {
        return R.id.nav_bottom_act_home
    }

    override fun onStart() {
        super.onStart()

        initui()
        init_navigation_linking()

    }


    // initialize all ui things
    fun initui(){

        detectKeyboard()

        // hide keyboard in the first time
        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

    }

    /**
     * On each touch event:
     * detect if user touched anywhere outside it's bounds and close keyboard
     */
    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        writeToLog("dispatchTouchEvent")

        KeyboardUtils.forceCloseKeyboard(window.decorView.rootView)

        // call super
        return super.dispatchTouchEvent(ev)
    }

    // detect keyboard showing in order to hide bottom navigation when the keyboard is showing
    fun detectKeyboard(){

        KeyboardUtils.addKeyboardToggleListener(
            this
        ) {
            if(it){
                navBottomView.visibility = View.GONE

            }else {
                navBottomView.visibility = View.VISIBLE

            }
        }

    }



    fun writeToLog(str: String){
        Log.e(TAG, str)
    }

    fun showCuisineFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, CuisinesFragment.newInstance())
        transaction.commit()
    }

    fun showMealsFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, MealsFragment.newInstance())
        transaction.commit()
    }

    fun showFiltersDialog() {
        var filtersDialog = FiltersDialog.newInstance()
        filtersDialog.show(supportFragmentManager, "filters_dialog")

    }


    fun init_navigation_linking(){

        btn_filters.setOnClickListener {
            showFiltersDialog()
        }
    }
}
