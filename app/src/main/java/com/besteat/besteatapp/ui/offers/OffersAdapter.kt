package com.besteat.besteatapp.ui.offers

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.text.Layout
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.*
import com.besteat.besteatapp.R
import com.besteat.besteatapp.models.Offer

class OffersAdapter(var offersList: ArrayList<Offer>) : RecyclerView.Adapter<OffersAdapter.ViewHolder>() {

    val TAG = "MealsAdapter"
    var onItemClick: ((Offer) -> Unit)? = null

    lateinit var context: Context
    var expandedItemPosition: Int? = null

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        Log.e(TAG, "onCreateViewHolder")

        context = p0.context
        val v = LayoutInflater.from(context).inflate(R.layout.cardview_offer, p0, false)
        return ViewHolder(v)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return offersList.size
    }


    //this method is binding the data on the list
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onBindViewHolder(holder: OffersAdapter.ViewHolder, position: Int) {
        writeLog("onBindViewHolder")
        holder.bindItems(offersList[position])
    }


    fun notifyItemExpand(position: Int) {
        writeLog("Item expand position : $position")

        if(expandedItemPosition == position)
            expandedItemPosition = null
        else
            expandedItemPosition = position

        notifyDataSetChanged()
    }

    fun writeLog(str: String) {
        Log.e(TAG, str)

    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val TAG2 = "OffersAdapter.ViewHolder"

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(offersList[adapterPosition])
            }
        }

        @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
        fun bindItems(offer: Offer) {
            fillListeItems(offer)
        }


        @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
        fun fillListeItems(offer: Offer) {
            writeLog2("fillListeItems")

            val btnExpand = itemView.findViewById(R.id.btn_expand) as ImageButton
            val btnShare = itemView.findViewById(R.id.btn_share) as ImageButton
            val expandedPart = itemView.findViewById(R.id.expanded_part) as ViewGroup

            var restoImg = itemView.findViewById(R.id.img_resto) as ImageView
            var title = itemView.findViewById(R.id.label_title) as TextView
            var deadline = itemView.findViewById(R.id.label_deadline) as TextView
            var restoName = itemView.findViewById(R.id.label_resto_name) as TextView
            var description = itemView.findViewById(R.id.label_description) as TextView

            // expand item clicked
            if (adapterPosition == expandedItemPosition) {
                expandedPart.visibility = View.VISIBLE
            }else {
                expandedPart.visibility = View.GONE
            }

            // expand button clicked
            btnExpand.setOnClickListener {
                writeLog2("btnExpand clicked!!!!!")
                notifyItemExpand(adapterPosition)
            }

            // fill data
            title.text = offer.title
            //description.text = offer.description


        }

        @SuppressLint("LongLogTag")
        fun writeLog2(str: String) {
            Log.e(TAG2, str)

        }
    }

}