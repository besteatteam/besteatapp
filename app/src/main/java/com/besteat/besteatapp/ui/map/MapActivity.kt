package com.besteat.besteatapp.ui.map

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.besteat.besteatapp.R
import com.besteat.besteatapp.ui.base.BaseActivity

class MapActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun getContentViewId(): Int {
        return R.layout.activity_map
    }

    override fun getNavigationMenuItemId(): Int {
        return R.id.nav_bottom_act_map
    }
}
