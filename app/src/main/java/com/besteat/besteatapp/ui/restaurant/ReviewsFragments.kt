package com.besteat.besteatapp.ui.restaurant

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.besteat.besteatapp.R
import com.besteat.besteatapp.models.Review
import com.besteat.besteatapp.ui.addReview.AddReviewActivity
import kotlinx.android.synthetic.main.fragment_restaurant_reviews.*

class ReviewsFragments: Fragment() {

    val TAG = "ReviewsFragments"

    lateinit var recyclerView: RecyclerView

    lateinit var adapter: ReviewsAdapter

    companion object {
        fun newInstance(): ReviewsFragments {
            return ReviewsFragments()
        }
    }

    override fun onStart() {
        super.onStart()
        writeToLog("onStart")

        init()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        writeToLog("onCreateView")

        return inflater.inflate(R.layout.fragment_restaurant_reviews, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        writeToLog("onViewCreated")

        recyclerView = view.findViewById(R.id.rvReviews) as RecyclerView
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)

        //creating our adapter
        adapter = ReviewsAdapter(ArrayList())

        //now adding the adapter to recyclerview
        recyclerView.adapter = adapter


        init_navigation_linking()
    }


    fun init() {
        writeToLog("init")

        var reviews: ArrayList<Review> = ArrayList()
        reviews.add(Review(2.5F,"adf","df", null,null))
        reviews.add(Review(3F,"adf","df", null,null))
        reviews.add(Review(3.5F,"adf","df", null,null))
        reviews.add(Review(4F,"adf","df", null,null))
        reviews.add(Review(4.5F,"adf","df", null,null))
        reviews.add(Review(5F,"adf","df", null,null))

        loadReviews(reviews)
    }

    fun loadReviews(reviews: ArrayList<Review>) {
        writeToLog( "loadCuisines")

        adapter.reviewList = reviews
        adapter.notifyDataSetChanged()

        adapter.onItemClick = {
            writeToLog("adapter.onItemClick : " + it.title)
        }
    }

    fun writeToLog(str: String){
        Log.e(TAG, str)
    }

    fun init_navigation_linking(){

        btn_addReview.setOnClickListener {
            context?.startActivity(Intent(context, AddReviewActivity::class.java))
        }

    }
}