package com.besteat.besteatapp.di.modules

import android.app.Activity
import com.besteat.besteatapp.di.scopes.PerActivity
import dagger.Module

@PerActivity
@Module
class ActivityModule(val activity: Activity) {

}