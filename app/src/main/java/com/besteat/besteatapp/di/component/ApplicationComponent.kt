package com.besteat.besteatapp.di.component

import com.besteat.besteatapp.App
import com.besteat.besteatapp.di.modules.ApplicationModule
import com.besteat.besteatapp.di.scopes.PerApplication
import dagger.Component


@PerApplication
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    fun inject(app: App)

}