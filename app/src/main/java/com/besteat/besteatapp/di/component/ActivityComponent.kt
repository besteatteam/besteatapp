package com.besteat.besteatapp.di.component

import com.besteat.besteatapp.di.modules.ActivityModule
import com.besteat.besteatapp.di.scopes.PerActivity
import dagger.Component

@PerActivity
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

}