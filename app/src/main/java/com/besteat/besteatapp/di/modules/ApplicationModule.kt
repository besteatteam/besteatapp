package com.besteat.besteatapp.di.modules

import com.besteat.besteatapp.App
import com.besteat.besteatapp.di.scopes.PerApplication
import dagger.Module

@PerApplication
@Module
class ApplicationModule(private val app: App){

}